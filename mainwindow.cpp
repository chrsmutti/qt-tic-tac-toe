#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->buttons[0] = ui->btnSlot1;
    this->buttons[1] = ui->btnSlot2;
    this->buttons[2] = ui->btnSlot3;
    this->buttons[3] = ui->btnSlot4;
    this->buttons[4] = ui->btnSlot5;
    this->buttons[5] = ui->btnSlot6;
    this->buttons[6] = ui->btnSlot7;
    this->buttons[7] = ui->btnSlot8;
    this->buttons[8] = ui->btnSlot9;

    for (int i = 0; i < 9; i++)
        connect(this->buttons[i], SIGNAL(clicked(bool)), this, SLOT(computeClick()));

    connect(ui->btnRestart, SIGNAL(clicked(bool)), this, SLOT(restart()));

    this->restart();
}

void MainWindow::computeClick() {
    QPushButton* button = qobject_cast<QPushButton*>(sender());

    for (int i = 0; i < 9; i++) {
        if (this->buttons[i] == button) {
            this->gameState[i] = this->currentPlayer;
            this->buttons[i]->setText(QString(this->getPlayerLabel()));
            this->buttons[i]->setDisabled(true);
        }
    }

    // diagonais
    if (this->gameState[0] && this->gameState[0] == this->gameState[4] &&
            this->gameState[0] == this->gameState[8])
        this->gameWon();
    if (this->gameState[2] && this->gameState[2] == this->gameState[4] &&
            this->gameState[2] == this->gameState[6])
        this->gameWon();

    // perpendiculares
    for (int i = 0; i < 3; i++) {
        if (this->gameState[i * 3] && this->gameState[i * 3] == this->gameState[(i * 3) + 1] &&
                this->gameState[i * 3] == this->gameState[(i * 3) + 2])
            this->gameWon();
        if (this->gameState[i] && this->gameState[i] == this->gameState[i + 3] &&
                this->gameState[i] == this->gameState[i + 6])
            this->gameWon();
    }

    int sum = 0;
    for (int i = 0; i < 9; i++)
        sum += this->gameState[i];

    if (sum > 12) {
        ui->lblWon->setText("It's a draw!");
        this->finished = true;
    }

    if (this->finished) {
        ui->lblTurn->setText("");
    } else {
        this->currentPlayer = (this->currentPlayer == 1) ? 2 : 1;
        this->setTurnLabel();
    }
}

void MainWindow::restart() {
    ui->lblWon->setText("");
    this->currentPlayer = 1;
    this->finished = false;
    this->setTurnLabel();

    for (int i = 0; i < 9; i++) {
        this->gameState[i] = 0;
        this->buttons[i]->setText("");
        this->buttons[i]->setEnabled(true);
        this->buttons[i]->setChecked(false);
    }
}

void MainWindow::gameWon() {
    this->finished = true;
    const char* playerLabel = this->getPlayerLabel();

    for (int i = 0; i < 9; i++) {
        this->buttons[i]->setEnabled(false);
        this->buttons[i]->setChecked(true);
    }

    ui->lblWon->setText(QString("Player %1 won!").arg(playerLabel));
}

void MainWindow::setTurnLabel() {
    const char* playerLabel = this->getPlayerLabel();

    ui->lblTurn->setText(QString("%1's turn.").arg(playerLabel));
}

const char* MainWindow::getPlayerLabel() {
    return (this->currentPlayer == 1) ? "X" : "O";
}

MainWindow::~MainWindow()
{
    delete ui;
}
