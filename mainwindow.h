#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QPushButton>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    int currentPlayer;
    int gameState[9] = { };
    bool finished;
    QPushButton* buttons[9] = { };
    Ui::MainWindow *ui;

    void setTurnLabel();
    const char*  getPlayerLabel();
    void gameWon();

public slots:
    void restart();
    void computeClick();
};

#endif // MAINWINDOW_H
